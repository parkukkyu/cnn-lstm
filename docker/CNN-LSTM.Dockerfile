FROM continuumio/anaconda3:2019.07
MAINTAINER parkukkyu <parkukkyu@hanmail.net>

ENV DEBIAN_FRONTEND noninteractive

ADD ./env/env.yml /cnn-lstm/env.yml

# excute dependicies
RUN ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime && \
    buildDeps='' && \
    excuteDeps='libcurl4-openssl-dev bzip2 curl wget vim htop' && \
    apt-get update && \
    apt-get install -y $buildDeps $excuteDeps --no-install-recommends && \
    /opt/conda/bin/conda env create -f /cnn-lstm/env.yml && \
    apt-get purge -y --auto-remove $buildDeps && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/*

# Copy Source
ADD ./model /cnn-lstm/model
ADD ./src /cnn-lstm/src
ADD ./instance /cnn-lstm/instance
ADD ./batch/server.sh /cnn-lstm/server.sh

# permission change
RUN chmod 755 /cnn-lstm/server.sh

RUN mkdir -p /cnn-lstm/download

EXPOSE 8000

# run server
CMD [ "/cnn-lstm/server.sh" ]
