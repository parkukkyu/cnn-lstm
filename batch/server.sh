#!/bin/bash

# CNN-LSTM service start
cd /cnn-lstm/src
/opt/conda/envs/cnn-lstm/bin/python3 api_server.py
