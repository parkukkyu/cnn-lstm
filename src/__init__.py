# -*- coding: utf-8-*-
import os
import sysconfig
os.environ['R_HOME'] = os.path.join(sysconfig.get_config_var('LIBDIR'), 'R')

import src.install_calibratR

from flask import Flask
from flask_restful import Api


__author__ = "Andrew Jeonguk Park"
__copyright__ = "Copyright 2019, Andrew J. Park. All rights reserved."
__email__ = "parkukkyu@hanmail.net"
__date__ = "2019/07/31"

app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('flask.cfg')
api = Api(app)
