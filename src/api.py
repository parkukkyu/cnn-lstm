# -*- coding: utf-8-*-
import numpy as np
import os
import urllib.request
import uuid

from flask import request, jsonify
from flask_restful import Resource

from src import app
from src.model import Model


__author__ = "Andrew Jeonguk Park"
__copyright__ = "Copyright 2019, Andrew J. Park. All rights reserved."
__email__ = "parkukkyu@hanmail.net"
__date__ = "2019/07/31"

model = Model(app.config['MODEL_DIR'])


# TODO(andrew j. park) 예외 처리 및 로그 추가
class Predict(Resource):
    def __init__(self):
        self.image_path = app.config['DOWNLOAD_IMAGES_DEST']


    @staticmethod
    def __make_x(request_data):
        return np.array([[request_data['age'], request_data['gender'],
                          request_data['mgmt'], request_data['idh'],
                          request_data['duration'], request_data['tdose'],
                          request_data['tfx']]])

    def __download_image(self, request_data, request_id):
        # TODO(andrew j. park) 병렬로 다운 받을 수 있도록 처리
        for index in range(9):
            file_name = request_id + '_' + str(index).zfill(4) + '.dcm'
            urllib.request.urlretrieve(
                request_data['mri_image_{}'.format(index + 1)],
                os.path.join(self.image_path, file_name))

    def __prepare_input(self, request_id):
        request_data = request.get_json()
        x = self.__make_x(request_data)
        self.__download_image(request_data, request_id)
        return x

    def post(self):
        request_id = str(uuid.uuid4())
        x = self.__prepare_input(request_id)
        value = model.predict(x, self.image_path, [request_id])
        return jsonify(output=value * 100.0)
