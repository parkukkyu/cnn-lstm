# -*- coding: utf-8-*-
import rpy2.robjects.packages as rpackages

__author__ = "Andrew Jeonguk Park"
__copyright__ = "Copyright 2019, Andrew J. Park. All rights reserved."
__email__ = "parkukkyu@hanmail.net"
__date__ = "2019/08/03"


utils = rpackages.importr('utils')
utils.install_packages('CalibratR', repos="http://cran.rstudio.com/")
