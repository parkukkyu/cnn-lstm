# -*- coding: utf-8-*-

__author__ = "Andrew Jeonguk Park"
__copyright__ = "Copyright 2019, Andrew J. Park. All rights reserved."
__email__ = "parkukkyu@hanmail.net"
__date__ = "2019/07/31"


class Singleton(type):
    __instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls.__instances:
            cls.__instances[cls] = super().__call__(*args, **kwargs)
        return cls.__instances[cls]
