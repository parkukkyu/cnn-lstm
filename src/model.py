# -*- coding: utf-8-*-
import numpy as np
import os
import pydicom
import tensorflow as tf

import rpy2.robjects as robjects
import rpy2.robjects.packages as rpackages

from sklearn.externals import joblib
from keras.models import model_from_json

from src.singleton import Singleton


__author__ = "Andrew Jeonguk Park"
__copyright__ = "Copyright 2019, Andrew J. Park. All rights reserved."
__email__ = "parkukkyu@hanmail.net"
__date__ = "2019/07/31"


class Model(object, metaclass=Singleton):
    model = None
    model_path = None
    scale = None
    frame_row = 256
    frame_col = 256
    channels = 1
    time_steps = 9
    FOV_row = 200
    FOV_col = 200
    calibratR = None
    calibration_model = None

    def __init__(self, model_path, scale=None):
        self.model_path = model_path
        if scale is None:
            self.scale = joblib.load(os.path.join(self.model_path,
                                                  'scale.save'))
        else:
            self.scale = scale
        self.__load_structure()
        self.__load_weights()
        self.__load_calibrat()

    def __load_structure(self):
        with open(os.path.join(self.model_path, 'structure.json'), 'rt') \
                as file:
            self.model = model_from_json(file.read())

    def __load_weights(self):
        self.model.load_weights(os.path.join(self.model_path, 'weights.hdf5'))

    def __load_calibrat(self):
        base = rpackages.importr('base')
        self.calibratR = rpackages.importr('CalibratR')

        base.load(os.path.join(self.model_path, 'calibratPython.RData'))
        self.calibration_model = robjects.globalenv['calibration_model'][0]

    def __pre_process(self, clinical, path, samples):
        clinical_x = self.__pre_process_clinical(clinical)
        mri_x = self.__pre_process_mri(path, samples)
        return clinical_x, mri_x

    def __pre_process_clinical(self, clinical):
        normalized_x = self.scale.transform(clinical)
        return normalized_x.reshape(normalized_x.shape[0],
                                    normalized_x.shape[1], 1)

    def __pre_process_mri(self, path, samples):
        data = np.zeros((len(samples), self.time_steps, self.frame_row,
                         self.frame_col))

        with tf.Session() as sess:
            for sample_index, sample in enumerate(samples):
                for index in range(self.time_steps):
                    file_name = '{}_{}.dcm'.format(sample, str(index).zfill(4))
                    dcm = pydicom.dcmread(os.path.join(path, file_name))
                    pixel_size = dcm.PixelSpacing[0]
                    img = dcm.pixel_array
                    img = img.reshape(img.shape[0], img.shape[1], 1)
                    img = img.astype(int)

                    img = tf.image.resize_image_with_crop_or_pad(img,
                                                                 int(round(self.FOV_row / pixel_size)),
                                                                 int(round(self.FOV_col / pixel_size)))
                    img = tf.image.resize_images(img, size=[self.frame_row,
                                                            self.frame_col],
                                                 method=tf.image.ResizeMethod.BICUBIC)
                    img = tf.image.per_image_standardization(img)
                    img_array = sess.run(img)
                    img_array = img_array.reshape(img.shape[0], img.shape[1])
                    data[sample_index, index, :, :] = img_array
        return data.reshape(data.shape[0], data.shape[1], data.shape[2],
                            data.shape[3], self.channels)

    def __calc_probability(self, score):
        return self.calibratR.predict_calibratR(self.calibration_model,
                                                new=score.item())[10][0]

    def predict(self, clinical, path, samples, batch_size=8):
        clinical_x, mri_x = self.__pre_process(clinical, path, samples)
        score = self.model.predict([mri_x, clinical_x], batch_size=batch_size)
        # return self.__calc_probability(score)
        probability = self.__calc_probability(score)
        print(probability)
        return probability
