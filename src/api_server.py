# -*- coding: utf-8-*-

import argparse
import os
import sys

sys.path.append(os.path.abspath(os.pardir))

from src import app, api
from src.api import Predict

__author__ = "Andrew Jeonguk Park"
__copyright__ = "Copyright 2019, Andrew J. Park. All rights reserved."
__email__ = "parkukkyu@hanmail.net"
__date__ = "2019/07/31"


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, default=8000)
    conf = parser.parse_args()

    api.add_resource(Predict, '/predict')
    app.run(host='0.0.0.0', port=conf.port, debug=False, threaded=False)
